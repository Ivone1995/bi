more off;
%...........................InputData..........................................%
sol.lag=15;
sol.p_train=0.25;
sol.p_test=1-sol.p_train;
sol.input="TSerie.txt";
sol.L=8; %Columnas
sol.h=7;
%...........................LoadData...........................................%
 
%data=load(sol.input);
 
data=[1:100]';
 
index=[1:rows(data)]';
 
%.......................NormalizeData..........................................%
%data=norm(data);
%grafico (data, 30,"ACF")
%.......................H-SVD Data.............................................%
sol=descomposition(sol,data);
%.......................SplitTrainTest.........................................%
 
%[x_train x_test]=train_test_split(data,sol.p_train);
[index_train index_test]=train_test_split(index,sol.p_train);
[sol.x_HTrain sol.x_HTest]=train_test_split(sol.x_H,sol.p_train);
[sol.x_LTrain sol.x_LTest]=train_test_split(sol.x_L,sol.p_train);
%sol.dataTrain=x_train;
%sol.dataTest=x_test;
 
%.......................RegressorVector........................................%
 
[xhreg yhreg]=regressor (sol.x_HTrain,sol.h,sol.lag);
[xlreg ylreg]=regressor (sol.x_LTrain,sol.h,sol.lag);
 
%.......................Training...............................................%
 
[yhpred ah]=AR(xhreg, yhreg);
 
[ylpred al]=AR(xlreg, ylreg);
 
sol.y_pred_test=ylpred+yhpred;
 
mse=MSE(ylreg+yhreg,sol.y_pred_test)
 
%.......................Testing................................................%
 
[xhreg2 yhreg2]=regressor (sol.x_H,0,sol.lag);
[xlreg2 ylreg2]=regressor (sol.x_L,0,sol.lag);
%sol=training_AAR(sol,sol.dataTrain);
 
%{
%Hacer calculo con data de testing
sol=testing_AAR(sol,sol.dataTest);
%Guardar datos de la solucion
save Model sol
%Hacer load best configuration
 
%.......................Testing................................................%
mse=MSE(sol.dataTest,sol.y_pred_test)
mae=MAE(sol.dataTest,sol.y_pred_test)
rmse=RMSE(sol.dataTest,sol.y_pred_test)
mape=MAPE(sol.dataTest,sol.y_pred_test)
r2=R2(sol.dataTest,sol.y_pred_test)
mnsc=mNSC(sol.dataTest,sol.y_pred_test) 
gcv=GCV(sol.dataTest,sol.y_pred_test,sol.L-1,rows(sol.dataTest))

%.......................Graficos...............................................%
 
figure(2)
plot (index_test,sol.dataTest,"-b");
hold on
plot (index_test,sol.y_pred_test,"*r");
legend('Actual Value','Estimated Value');
hold off
grid on
xlabel ("Time");
ylabel ("Normalized Catches");
%title ("Simple 2-D Plot"); 
%figure(2)
%plot (sol.dataTrain,sol.y_pred,"o");
sol.y_pred_test=sol.dataTest;

figure(3)
%>> X = [ones(rows(sol.dataTest), 1) sol.dataTest];
%>> theta = (pinv(X'*X))*X'*sol.y_pred_test
X=[ones(rows(sol.dataTest), 1) sol.dataTest];
theta = (pinv(X'*X))*X'*sol.y_pred_test
plot(sol.dataTest,sol.y_pred_test,'o','MarkerSize',10);
% Plot the fitted equation we got from the regression
hold on; % this keeps our previous plot of the training data visible
plot(X(:,2), X*theta, '-')
plot(sol.dataTest,sol.dataTest,"b...")
%ylim([0 1]);
legend('Training data', 'Linear regression','Y=X')
hold off % Don't put any more plots on this figure
xlabel ("Target X");
ylabel ("Y: Linear Fit"); 
%}

 