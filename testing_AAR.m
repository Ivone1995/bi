## Copyright (C) 2018 Kyare
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} testing_AAR (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn



function [sol] = testing_AAR (sol, data)
 %...........................Hankel.............................................%

  H=Hankl(data,sol.L);
  sol.H_test=H;
  %.............................SVD&C............................................%
  %SVD y calculos de C
  [u,s,v]=svd(H);
  H_usv=u*s*v';
  aux=0;
  s_t=s';
  s_diag=diag(s_t);
  %display("SSSSSSSSS");
  u_t=u';
  %display("UUUUUU");
  v_t=v';
  %display("VVVVVVV");
  C = [];
  for i=1:max(size(s_diag))
    %display(s_diag(i))
    %display(u_t(i,:))
    %display(v_t(i,:))
    aux = s_diag(i)*u_t(i,:)'*v_t(i,:);
    %otro = [aux(1,:) aux(:,size(aux)(2))'];
    otr = [aux(1,:) aux(2:rows(s_diag),size(aux)(2))'];
    C = [C; aux(1,:) aux(2:rows(s_diag),size(aux)(2))'];
  endfor
  %.......................Prediction.............................................%
  sol.C_test=C;
  x_L = C(1,:);
  x_H = sum(C(2:rows(C),:),1); 
  %[y_L a_l]=AR (x_L , data,sol);
  y_L=x_L*sol.a_l;
  %sol.x_H=x_H;
  %sol.x_L=x_L;
  %[y_H a_h]=AR (x_H , data,sol);
  y_H=x_H*sol.a_h;
  y_pred=y_L+y_H;
  sol.y_pred_test=y_pred';
  %sol.a_h=a_h;
  %sol.a_l=a_l;
endfunction
