more off;

%...........................InputData..........................................%
sol.memoria=7; #Definir el lag
sol.p_train=0.75; #Porcentaje training
sol.p_test=1-sol.p_train; #Procentaje testing
sol.input="dataProfe.txt"; #Leer la data
#sol.L=8; %filas
sol.h=1; %Horizonte

%...........................LoadData...........................................%
data=load(sol.input);

%.......................NormalizeData..........................................%
data=norm(data);
bestMemoria = []; 
bestLMse = 99999; #El valor optimo es: 
bestMae = 99999; #El valor optimo es: 
besth=1;
bestah = [];
bestal =[]; 
bestMNSC = -9; #El valor optimo es: 1
bestR2 = 0; #El valor optimo es: 
bestL = [];
Lhank = [5:10]; #Dejarlo en rangos
h = [1]; #Dejarlo en rangos
memoria = [28 30 32 34]; #Solo se ocupa en el modelo AR, dejarlo en rangos
%.......................H-SVD Data.............................................%
#for Lhank=2:round(rows(data)/2) 
for i=1:length(Lhank) 
  #printf("El Lhank es: %d \n", Lhank(i));
  for k=1:length(memoria)
    #printf("El horizonte es: %d \n", h(j))
    for j=1:length(h)
      printf("EL indice de l es: %d y el valor es: %d \n", i, Lhank(i));
      printf("EL indice de memoria es: %d  y el valor es: %d \n",k, memoria(k));
      #printf("la memoria es: %d \n", memoria(k))
      sol=descomposition(sol,data,Lhank(i)); 


      %.......................SplitTrainTest.........................................%
      [sol.x_HTrain sol.x_HTest]=train_test_split(sol.x_H,sol.p_train);
      [sol.x_LTrain sol.x_LTest]=train_test_split(sol.x_L,sol.p_train);


      %.......................RegressorVector........................................%
      [xhreg yhreg]=regressor (sol.x_HTrain,h(j),memoria(k));
      [xlreg ylreg]=regressor (sol.x_LTrain,h(j),memoria(k));

      %.......................Training...............................................%
      [yhpred ah]=AR(xhreg, yhreg);
      [ylpred al]=AR(xlreg, ylreg);
      sol.y_pred_test=ylpred+yhpred;
      mse=MSE(ylreg+yhreg,sol.y_pred_test);
      #mse=MSE(sol.dataTest,sol.y_pred_test)
      mae=MAE(ylreg+yhreg,sol.y_pred_test);
      rmse=RMSE(ylreg+yhreg,sol.y_pred_test);
      mape=MAPE(ylreg+yhreg,sol.y_pred_test);
      r2=R2(ylreg+yhreg,sol.y_pred_test);
      mnsc=mNSC(ylreg+yhreg,sol.y_pred_test);
      gcv=GCV(ylreg+yhreg,sol.y_pred_test,Lhank(i)-1,rows(ylreg+yhreg));
      #printf("EL MAPE ES: %d \n", mape);
      #printf("EL GCV ES: %d \n", gcv);
      #printf("El valor de bestmse es: %d \n", bestLMse);
      #if(mse < bestLMse && mae < bestMae && r2 > bestR2 && mnsc > bestMNSC)
      if(mnsc > bestMNSC)
        #display("ESTOY AQUI!!!!");
        bestLMse = mse;
        bestMae = mae;
        bestR2 =r2;
        bestMNSC =mnsc;
        bestYPredTest= sol.y_pred_test;
        bestXLTrain = sol.x_LTrain;
        bestXHTrain = sol.x_HTrain; 
        bestXLTest = sol.x_LTest;
        bestXHTest = sol.x_HTest;
        bestah = ah;
        bestal =al;
        #bestL = Lhank(i);
        #bestMemoria = memoria(k);
        bestL = i;
        bestMemoria = k;
        printf("El mejor Lhank primera parte es: %d \n", Lhank(bestL));
        printf("La mejor memoria primera partees: %d \n", memoria(bestMemoria));
      endif
   endfor
  endfor
endfor
%...........................Segundo proceso de training ........................%
#Se tiene varios horizontes para el mejor m y l encontrados 
h = [0:20];
bestH = [];
bestMNSC = -9;
  for ho=1:length(h)
    #printf("la memoria es: %d \n", memoria(k))
      sol=descomposition(sol,data,Lhank(bestL)); 


      %.......................SplitTrainTest.........................................%
      [sol.x_HTrain sol.x_HTest]=train_test_split(sol.x_H,sol.p_train);
      [sol.x_LTrain sol.x_LTest]=train_test_split(sol.x_L,sol.p_train);


      %.......................RegressorVector........................................%
      [xhreg yhreg]=regressor (sol.x_HTrain,h(ho),bestMemoria);
      [xlreg ylreg]=regressor (sol.x_LTrain,h(ho),bestMemoria);

      %.......................Training...............................................%
      [yhpred ah]=AR(xhreg, yhreg);
      [ylpred al]=AR(xlreg, ylreg);
      sol.y_pred_test=ylpred+yhpred;
      mse=MSE(ylreg+yhreg,sol.y_pred_test);
      #mse=MSE(sol.dataTest,sol.y_pred_test)
      mae=MAE(ylreg+yhreg,sol.y_pred_test);
      rmse=RMSE(ylreg+yhreg,sol.y_pred_test);
      mape=MAPE(ylreg+yhreg,sol.y_pred_test);
      r2=R2(ylreg+yhreg,sol.y_pred_test);
      mnsc=mNSC(ylreg+yhreg,sol.y_pred_test);
      gcv=GCV(ylreg+yhreg,sol.y_pred_test,Lhank(i)-1,rows(ylreg+yhreg));
      #printf("El valor de bestmse es: %d \n", bestLMse);
      VectorPlot(bestL, bestMemoria, ho)= mnsc; #i=lhank k=memoria j=h
      #if(mse < bestLMse && mae < bestMae && r2 > bestR2 && mnsc > bestMNSC)
      if(mnsc > bestMNSC)
        bestLMse2 = mse;
        bestMae2 = mae;
        bestR22 =r2;
        bestMNSC =mnsc;
        bestYPredTest2= sol.y_pred_test;
        bestXLTrain2 = sol.x_LTrain;
        bestXHTrain2 = sol.x_HTrain; 
        bestXLTest2 = sol.x_LTest;
        bestXHTest2 = sol.x_HTest;
        bestah2 = ah;
        bestal2 =al;
        bestH = h(ho);
        printf("El mejor valor de h es: %d \n", h(ho));
        printf("El mejor valor de la memoria es: %d \n", memoria(bestMemoria));
        printf("EL mejor valor de L es: %d \n", Lhank(bestL));
        printf("Los mejor valores de mnsc es: %d \n", bestMNSC);
      endif
  endfor 
  
 %----------------------Normalización vs horizonte--------------------------%
#l = 1; %indice del l
#VectorPlot(i,k,j)= mnsc; #i=lhank k=memoria j=h
vec1 = VectorPlot(bestL,bestMemoria,:);
vec2 = VectorPlot(bestL,bestMemoria,:);
vec3 = VectorPlot(bestL,bestMemoria,:);
vec4 = VectorPlot(bestL,bestMemoria,:);

figure(4)
plot(h,vec1,"--b");
hold on;
plot(h,vec2,"-b");

plot(h,vec3,"s");

plot(h,vec4,"+");
hold off;
xlabel ("Horizonte");
ylabel ("mNSC");
 %----------------------Normalización vs horizonte--------------------------%

 
 %.............................Testing..........................................%
  [xhregTest yhregTest]=regressor (bestXHTest,h(besth),memoria(bestMemoria));
  [xlregTest ylregTest]=regressor (bestXLTest,h(besth),memoria(bestMemoria));

  yhpredTest = xhregTest*bestah;
  ylpredTest = xlregTest*bestal;
  sol.y_pred_test=ylpredTest+yhpredTest;
  mse=MSE(ylregTest+yhregTest,sol.y_pred_test);
  
 
%.......................Plot Testing..........................................%
figure(2)
plot (ylregTest+yhregTest,"-b");
hold on
plot (sol.y_pred_test,"*r");
legend('Actual Value','Estimated Value');
hold off
grid on
xlabel ("Time");
ylabel ("Normalized Catches");
%title ("Simple 2-D Plot");
%figure(2)
%plot (sol.dataTrain,sol.y_pred,"o");


figure(3)
%>> X = [ones(rows(sol.dataTest), 1) sol.dataTest];
%>> theta = (pinv(X'*X))*X'*sol.y_pred_test
sol.dataTest=ylregTest+yhregTest;
X=[ones(rows(sol.dataTest), 1) sol.dataTest];
theta = (pinv(X'*X))*X'*sol.y_pred_test
plot(sol.dataTest,sol.y_pred_test,'o');
% Plot the fitted equation we got from the regression
hold on; % this keeps our previous plot of the training data visible
plot(X(:,2), X*theta, '-')
plot(sol.dataTest,sol.dataTest,"b--")
%ylim([0 1]);
legend('Training data', 'Linear regression','Y=X')
hold off % Don't put any more plots on this figure
xlabel ("Target X");
ylabel ("Y: Linear Fit");


