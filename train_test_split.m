function [x_train , x_test]= train_test_split(M,p_train)
  data_columns=columns(M);
  train_size=round(rows(M)*p_train);
  if(columns(M)>1)
  a=1
  %train_size=rows(M)*p_train;
  %test_size=rows(M)*p_test;
  x_train = M(1:train_size,1:(data_columns-1))
  x_test = M((train_size+1):end,1:(data_columns-1))
  
  %y_train = M(1:train_size,(data_columns));
  %y_test = M((train_size+1):end,(data_columns));
  else
  
  %test_size=rows(M)-train_size;
  x_train = M(1:train_size,1);
  x_test = M((train_size+1):end,1);
  endif
  
end